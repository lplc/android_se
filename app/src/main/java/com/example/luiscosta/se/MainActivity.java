package com.example.luiscosta.se;

import android.app.FragmentManager;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;

import com.example.luiscosta.se.R;
import com.example.luiscosta.se.on_off_Fragment;
import com.example.luiscosta.se.reservation_Fragment;
import com.google.firebase.messaging.FirebaseMessaging;



public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    SharedPreferences prefs = null;

    public void login() {
        prefs = getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);
        prefs.edit().putString("user", "").commit();

        Log.d("url", "passou na atividade");



        Log.d("url_base",prefs.getString("link",""));


        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);




    }

    public String getUser() {
        return prefs.getString("user","no_user");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FirebaseMessaging.getInstance().subscribeToTopic("allDevices");

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, new on_off_Fragment() ).commit();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Log.d("logout","yes");
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        FragmentManager fragmentManager = getFragmentManager();

        if (id == R.id.nav_on_off) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new on_off_Fragment() ).commit();
        }
        else if (id == R.id.nav_reservation) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new reservation_Fragment() ).commit();

        }
        else if (id == R.id.nav_values) {
            fragmentManager.beginTransaction().replace(R.id.content_frame, new values_Fragment() ).commit();
        }
        else if( id == R.id.action_settings){
            Log.d("login", "go to login");
            login();
        }

        Log.d("menu_id",String.valueOf(id));
        Log.d("menu_settings",String.valueOf(R.id.action_settings));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    public void logout(){

        Log.d("logout","Click");

        prefs = getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);
        prefs.edit().putString("user", "").commit();


        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }
}
