package com.example.luiscosta.se;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by luiscosta on 17/04/18.
 */



public class on_off_Fragment extends Fragment {

    View myView;
    double temp=50;
    String connected="off";
    SharedPreferences prefs = null;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView=inflater.inflate(R.layout.on_off,container,false);

        prefs=this.getActivity().getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);



        put_values();
        state();
        turn_on(false);

        final Button button= myView.findViewById(R.id.btn_ligar_chaleira);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                turn_on(true);
                Context context = getActivity();
                CharSequence text = "The kettle is connected";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

            }
        });

        return myView;
    }

    public void state(){
        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","")  + "/state";

        Log.d("url",url);

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //Log.e("temperatura", response.get(0).toString() );
                            //Log.e("quantidade disponivel", response.get(1).toString() );
                            //Log.e("quantidade total", response.get(2).toString() );
                            //Log.e("quantidade minima", response.get(3).toString() );
                            //temp=(int)response.get(0);
                            temp = (double) response.get(0);
                            if(response.get(4).equals("s")){
                                Button btn = myView.findViewById(R.id.btn_ligar_chaleira);
                                btn.setEnabled(false);
                                connected=" connected";
                            }
                            else
                                connected= " not connected";

                            TextView con = myView.findViewById(R.id.connected);

                            con.setText( getResources().getString(R.string.connected) + connected);

                            put_values();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("tag", error.toString() );

                    }
                });
        queue.add(jsonObjectRequest);

    }

    public void put_values(){
        TextView text = (TextView) myView.findViewById(R.id.on_off_temp);
        String someString = getResources().getString(R.string.temp_atual);
        text.setText(someString + " " + temp+"ºC");
    }


    public void turn_on(final Boolean con){

        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","")  + "/turn_on?con="+con;

        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        // Display the first 500 characters of the response string.
                        Log.d("conn", response);
                        Context context = getActivity();
                        CharSequence text="";
                        int duration = Toast.LENGTH_LONG;
                        if(response.equals("s")){
                            Button btn = myView.findViewById(R.id.btn_ligar_chaleira);
                            btn.setEnabled(false);
                            if(con)
                                text = "Kettle turned on, you will be warned when the water is ready";
                            else
                                text = "Kettle is currently connected";
                        }
                        else if(response.equals("p")){
                            text = "Kettle does not have a minimum water level to turn on";
                        }
                        Toast toast = Toast.makeText(context, text, duration);
                        toast.show();
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("conn", "error");
            }
        });

        queue.add(stringRequest);

    }

}
