package com.example.luiscosta.se;

import android.app.DownloadManager;
import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by luiscosta on 17/04/18.
 */

public class reservation_Fragment extends Fragment {

    double temp=50;
    double quant_avail=200;
    double quant_total=500;
    double min=25;
    double current_value=min;
    String connected="Off";
    View myView;
    SeekBar seek_bar;
    TextView seek_bar_value;
    SharedPreferences prefs = null;
    String my_user="";


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView=inflater.inflate(R.layout.reservation,container,false);
        prefs=this.getActivity().getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);

        history();

        final Button button= myView.findViewById(R.id.button2);

        //put_values();

        state();

        prefs=this.getActivity().getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);
        my_user=prefs.getString("user","no_user");


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //final TextView mTextView = (TextView) myView.findViewById(R.id.textView2);
                int quantity=200;
                RequestQueue queue;
                queue= Volley.newRequestQueue(getActivity());
                String url =prefs.getString("link","") +"/reserve?token=" + FirebaseInstanceId.getInstance().getToken() +"&user="+ my_user +"&qt="+ (int) current_value ;

                Context context = getActivity();
                CharSequence text = "Reservation made with success for "+ current_value +"ml";
                int duration = Toast.LENGTH_LONG;

                Toast toast = Toast.makeText(context, text, duration);
                toast.show();

                JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                        (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                            @Override
                            public void onResponse(JSONArray response) {
                                Log.e("tag", response.toString() );
                                //possible answers
                                //Done
                                //No Water Available
                                if(response.toString().equals("Done")){
                                    //water reserved

                                    button.setEnabled(false);
                                }

                                state();
                                Context context = getActivity();
                                CharSequence text = response.toString();
                                int duration = Toast.LENGTH_LONG;

                                Toast toast = Toast.makeText(context, text, duration);
                                toast.show();
                                history();

                                //mTextView.setText("Response: " + response.toString() );
                            }
                        }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {
                                // TODO: Handle error
                                Log.e("tag", error.toString() );
                            }
                        });

                // Add the request to the RequestQueue.
                queue.add(jsonObjectRequest);

            }
        });

        seek_bar = (SeekBar) myView.findViewById(R.id.reservation_seekBar);
        seek_bar.setMin((int) (min/5));
        seek_bar.setMax((int) (quant_avail+min)/5);
        seek_bar.setProgress((int) (min/5));
        current_value=min;
        seek_bar_value = (TextView) myView.findViewById(R.id.value_seek_bar);
        seek_bar_value.setText(""+current_value);



        seek_bar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {
                progress = progress * 5;
                current_value = progress;
                seek_bar_value = (TextView) myView.findViewById(R.id.value_seek_bar);
                seek_bar_value.setText(""+current_value);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        return myView;
    }

    public void state(){
        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","")  + "/state";

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @RequiresApi(api = Build.VERSION_CODES.O)
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            //Log.e("temperatura", response.get(0) );
                            //Log.e("quantidade disponivel", response.get(1).toString() );
                            //Log.e("quantidade total", response.get(2).toString() );
                            //Log.e("quantidade minima", response.get(3).toString() );
                            temp = (double) response.get(0);
                            quant_avail=(double) response.get(1);
                            quant_total=(double) response.get(2);
                            min=(double) response.get(3);
                            if(response.get(4).equals("s")){
                                connected="On";
                                Button connect = myView.findViewById(R.id.button2);
                                connect.setEnabled(false);
                            }
                            else{
                                connected="Off";
                                Button connect = myView.findViewById(R.id.button2);
                                connect.setEnabled(true);
                            }

                            seek_bar= (SeekBar) myView.findViewById(R.id.reservation_seekBar);
                            seek_bar.setMax((int) (quant_avail-min));
                            seek_bar.setMin((int) (min/5));
                            seek_bar.setProgress((int) (current_value-min));


                            seek_bar_value = (TextView) myView.findViewById(R.id.value_seek_bar);
                            seek_bar_value.setText("");
                            Log.d("teste","1");
                            put_values();
                            Log.d("teste","2");
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("tag", error.toString() );

                    }
                });
        queue.add(jsonObjectRequest);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    public void put_values(){
        Log.d("Entrou","aqui");
        final TextView table_1 = (TextView)  myView.findViewById(R.id.reservation_tempeature);
        table_1.setText(String.valueOf(temp) +" ºC");

        final TextView table_2 = (TextView)  myView.findViewById(R.id.reservation_water_available);
        table_2.setText(String.valueOf(quant_avail)+" ml");

        final TextView table_3 = (TextView)  myView.findViewById(R.id.reservation_water_total);
        table_3.setText(String.valueOf(quant_total) +" ml");

        final TextView table_4 = (TextView)  myView.findViewById(R.id.reservation_minimum);
        table_4.setText(String.valueOf(min) +" ml");

        final TextView table_5 = myView.findViewById(R.id.state_kettle);
        table_5.setText(connected);


        seek_bar = (SeekBar) myView.findViewById(R.id.reservation_seekBar);
        seek_bar.setMin((int) (min/5));
        seek_bar.setMax((int) (quant_avail+min)/5);
        seek_bar.setProgress((int) (min/5));
        current_value=min;
        seek_bar_value = (TextView) myView.findViewById(R.id.value_seek_bar);
        seek_bar_value.setText(""+current_value);

    }

    public void history(){
        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","") +"/history";

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.e("one", response.get(0).toString() );
                            String history= response.get(0).toString();

                            Log.d("history",history);
                            if(history.toLowerCase().contains(my_user.toLowerCase())){
                                final Button button= myView.findViewById(R.id.button2);
                                button.setEnabled(false);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("tag", error.toString() );
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }



}
