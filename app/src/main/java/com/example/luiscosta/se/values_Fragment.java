package com.example.luiscosta.se;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by luiscosta on 17/04/18.
 */

public class values_Fragment extends Fragment {

    View myView;

    TableLayout t1;

    SharedPreferences prefs = null;

    private String localjsonString = "{\"country\":[{\"country_name\":\"country1\",\"capital\":\"capital1\"},{\"country_name\":\"country2\",\"capital\":\"capital2\"},{\"country_name\":\"country3\",\"capital\":\"capital3\"}," +
            "{\"country_name\":\"country4\",\"capital\":\"capital4\"},{\"country_name\":\"country5\",\"capital\":\"capital5\"},{\"country_name\":\"country6\",\"capital\":\"capital6\"}," +
            "{\"country_name\":\"country7\",\"capital\":\"capital7\"},{\"country_name\":\"country8\",\"capital\":\"capital8\"},{\"country_name\":\"country9\",\"capital\":\"capital9\"}," +
            "{\"country_name\":\"country10\",\"capital\":\"capital10\"}]}";

    private String teste2 = "{\"reserves\" : [{\"name\":\"user1\",\"quantity\":\"200\"}, {\"name\":\"user2\",\"quantity\":\"300\"}] }";

    private String history ;

    JSONArray teste = new JSONArray();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myView=inflater.inflate(R.layout.values,container,false);

        prefs = this.getActivity().getSharedPreferences("com.example.luiscosta.se", MODE_PRIVATE);

        try {
            request();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Button water_collected= myView.findViewById(R.id.water_collected);
        water_collected.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Log.d("coll","1");
                colect_water();
            }
        });



        return myView;
    }

    public void create_table() throws JSONException {
        initList();
        ListView mylist = (ListView) myView.findViewById(R.id.values);
        SimpleAdapter simpleAdapter = new SimpleAdapter(getActivity(), output, android.R.layout.simple_list_item_1, new String[]{"reserve"}, new int[]{android.R.id.text1});
        mylist.setAdapter(simpleAdapter);


    }

    List<Map<String, String>> output = new ArrayList<Map<String, String>>();

    private void initList() {
        try {
            JSONObject jsonResponse = new JSONObject(history);
            JSONArray jsonMainNode = jsonResponse.optJSONArray("reserves");
            for (int i = 0; i < jsonMainNode.length(); i++) {
                JSONObject jsonChildNode = jsonMainNode.getJSONObject(i);
                String name = jsonChildNode.optString("user");
                String number = jsonChildNode.optString("qt");
                String outPut = name + "                    " + number+"ml";
                output.add(createEmployee("reserve", outPut));
            }
        } catch (JSONException e) {
            Toast.makeText( getActivity(), "Error" + e.toString(), Toast.LENGTH_SHORT).show();
        }
    }

    private HashMap<String, String> createEmployee(String name, String number) {
        HashMap<String, String> employeeNameNo = new HashMap<String, String>();
        employeeNameNo.put(name, number);
        return employeeNameNo;
    }

    public void populate() throws JSONException {
        JSONObject jo = new JSONObject();

        jo.put("user", "user1");
        jo.put("quantity", "200");
        teste.put(jo);

        jo.put("user", "user2");
        jo.put("quantity", "300");
        teste.put(jo);

        jo.put("user", "user3");
        jo.put("quantity", "50");
        teste.put(jo);

    }


    public void request() throws JSONException {
        history="";
        output.clear();
        populate();
        create_table();

        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","")+"/history";

        JsonArrayRequest jsonObjectRequest = new JsonArrayRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.e("one", response.get(0).toString() );
                            history= "{\"reserves\" :".concat(response.get(0).toString()).concat("}");

                            String to_verify=response.get(0).toString();

                            Log.d("history",history);

                            try {
                                populate();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                            try {
                                create_table();
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            String my_user=prefs.getString("user","no_user");
                            final Button button= myView.findViewById(R.id.water_collected);

                            if(to_verify.toLowerCase().contains(my_user.toLowerCase())){
                                button.setEnabled(true);
                            }
                            else{
                                button.setEnabled(false);
                            }

                            //Log.e("one_", response.get(1).toString() );
                            //Log.e("quantidade total", response.get(2).toString() );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        // mTextView.setText("Response: " + response.toString() );
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO: Handle error
                        Log.e("tag", error.toString() );
                    }
                });

        // Add the request to the RequestQueue.
        queue.add(jsonObjectRequest);
    }



    public void colect_water(){
        Log.d("coll","2");

        RequestQueue queue;
        queue= Volley.newRequestQueue(getActivity());
        String url =prefs.getString("link","")+"/collect?token=" + FirebaseInstanceId.getInstance().getToken();
        Log.d("coll_link",url);


        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("collect",response);
                        try {
                            request();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("coll_con", "error");
            }
        });


        queue.add(stringRequest);

    }

}




